## Setting up Docker

### Change host name of database server

Because we use a docker container for database server so we need to update the host name in some configuration files:

- ./../configuration/config.connection.json
- ./../configuration/log4php_config.xml

### Update domain

Because of using Docker for web server, we need to add a domain mapping on your local machine.

- Add below line to `/etc/hosts` (your local machine):
```
    127.0.0.1   dev.comet.ws.jw
```

### Docker information

We used docker-composer to run multiple-containers for mysql and web server.

#### Mysql

- `Container`: comet_ws_jw-mysql
- `Version`: 5.7
- `Service name`: db
- More information can be found in [docker-compose](docker-compose.yml) file.

#### Web server

- `Container`: comet_ws_jw-apache
- `Version`: apache 2 with php5.5
- `Service name`: web
- More information can be found in [docker-compose](docker-compose.yml) file.

#### Run Containers

- Using below command line to run containers with `docker-compose`

```
//access to docker foler
cd docker

//run containers in the background and check to rebuild images
docker-compose up -d --build
```

### Import database for Database container

After starting the db container for the first time, we need to import database (detail guideline can be found in [here](http://wiki.laudert.intra/display/PRINT/PHP+Projects))

### Debug with xDebug and IntelliJ IDEA/phpStorm

Almost of configurations for docker web server have been prepared already. One thing you need to do is configure your local ip address as an environment variable (add `.env` file).

```
XDEBUG_HOST_IP={YOUR_IP_ADDRESSS}
```   

[Reference](https://www.arroyolabs.com/2016/10/docker-xdebug/);